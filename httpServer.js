const http = require("http");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((req, res) => {
  try {
    if (req.method === "GET" && req.url === "/html") {
      getHtml(res);
    } else if (req.method === "GET" && req.url === "/json") {
      getJson(res);
    } else if (req.method === "GET" && req.url === "/uuid") {
      getUuid(res);
    } else if (req.method === "GET" && req.url.includes("/status")) {
      getStatusCode(req, res);
    } else if (req.method === "GET" && req.url.includes("/delay")) {
      getDelayed(req, res);
    } else {
      res.writeHead(404, { "Content-Type": "text/plain" });
      res.end(`Bad Request- URL not Found!!!`);
    }
  } catch (error) {
    res.writeHead(500, { "Content-Type": "text/plain" });
    res.end(`Something Went Wrong!!! ${error.message}`);
  }
});

server.on("error", (err) => {
  console.log(err.message);
});

server.listen(8000, () => {
  console.log(`listening on port 8000`);
});

// function to return the html content
const getHtml = (res) => {
  res.setHeader("Content-type", "text/html");
  res.end(`<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
      <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
      <p> - Martin Fowler</p>

  </body>
</html>`);
};

//function to display the JSON content
const getJson = (res) => {
  res.setHeader("Content-type", "application/json");
  res.end(
    JSON.stringify({
      slideshow: {
        author: "Yours Truly",
        date: "date of publication",
        slides: [
          {
            title: "Wake up to WonderWidgets!",
            type: "all",
          },
          {
            items: [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets",
            ],
            title: "Overview",
            type: "all",
          },
        ],
        title: "Sample Slide Show",
      },
    })
  );
};

// function to display the uuid
const getUuid = (res) => {
  const uuid = { uuid: uuidv4() };
  res.setHeader("Content-type", "application/json");
  res.end(JSON.stringify(uuid));
};

// function to respond with a specified status code

const getStatusCode = (req, res) => {
  let url = req.url.split("/");
  let index = url.length - 1;
  let code = parseInt(url[index]);
  //console.log(code);
  if (http.STATUS_CODES[code]) {
    res.statusCode = code;
    res.writeHead(url[index], { "Content-Type": "text/plain" });
    res.end(`${http.STATUS_CODES[code]}`);
    // console.log(`${http.STATUS_CODES[code]}`);
  } else {
    res.writeHead(422, { "Content-Type": "text/plain" });
    res.end(`${http.STATUS_CODES["422"]}`);
  }
};

// function to get response after given delay

const getDelayed = (req, res) => {
  let url = req.url.split("/");
  let index = url.length - 1;
  let delay = url[index];
  //console.log(delay);
  if (delay) {
    if (delay < 5) {
      setTimeout(() => {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(`delayed response by ${delay} seconds`);
      }, delay * 1000);
    } else {
      res.writeHead(400, { "Content-Type": "text/plain" });
      res.end(`${http.STATUS_CODES["400"]}`);
    }
  } else {
    res.writeHead(422, { "Content-Type": "text/plain" });
    res.end(`${http.STATUS_CODES["422"]}`);
  }
};
